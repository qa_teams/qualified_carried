package qualified_carried.questions;

import static net.serenitybdd.screenplay.questions.Text.of;
import static qualified_carried.userinterface.MyCarrierPage.LISTA_FILTER;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;

@Subject("{0} Found in the list")
public class Fond implements Question<String> {

	private String carriersName = "carrier name";

	public Fond(String carriersName) {
		this.carriersName = carriersName;

	}

	@Override
	public String answeredBy(Actor actor) {
		return of(LISTA_FILTER.of(carriersName)).viewedBy(actor).asString();
	}

	public static Fond encontrado(String carriersName) {
		return new Fond(carriersName);

	}

}
