package qualified_carried.questions;

import static net.serenitybdd.screenplay.questions.Text.of;
import static qualified_carried.userinterface.MyCarrierPage.LABEL_MESSAGE;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;

@Subject("the confirmation message")
public class TheMessage implements Question<String> {

	private String message = "confirmation message";

	public TheMessage(String message) {
		this.message = message;
	}

	@Override
	public String answeredBy(Actor actor) {
		return of(LABEL_MESSAGE.of(message)).viewedBy(actor).asString();

	}

	public static TheMessage content(String message) {
		return new TheMessage(message);

	}

}
