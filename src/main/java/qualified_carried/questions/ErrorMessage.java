package qualified_carried.questions;

import static net.serenitybdd.screenplay.questions.Text.of;
import static qualified_carried.userinterface.LoginPage.LABEL;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;

public class ErrorMessage implements Question<Boolean> {

	private String mensajeError;

	public ErrorMessage(String mensajeError) {
		this.mensajeError = mensajeError;

	}

	@Override
	@Subject("{0} se validaria el mensaje de error")
	public Boolean answeredBy(Actor actor) {
		return of(LABEL).viewedBy(actor).asString().contains(mensajeError);

	}

	public static ErrorMessage errorMessage(String mensajeError) {
		return new ErrorMessage(mensajeError);

	}

}
