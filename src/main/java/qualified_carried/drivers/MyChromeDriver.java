package qualified_carried.drivers;

import static java.util.concurrent.TimeUnit.SECONDS;
import static qualified_carried.utils.Constantes.ADD_ARGUMENTS_MAXIMIZED;
import static qualified_carried.utils.Constantes.SET_PRO_CHROME;
import static qualified_carried.utils.Property.getChromeDriver;
import static qualified_carried.utils.Property.getPRO;
import static qualified_carried.utils.Property.getURL;
import static qualified_carried.utils.Property.load;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MyChromeDriver {

	public static WebDriver driver = null;

	public static WebDriver getDriver() {
		return driver;
	}

	public static WebDriver initChromeDriver() {
		load();
		System.setProperty(SET_PRO_CHROME, getPRO().getProperty(getChromeDriver()));
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments(ADD_ARGUMENTS_MAXIMIZED);
		driver = new ChromeDriver(chromeOptions);
		driver.manage().timeouts().implicitlyWait(15, SECONDS);
		driver.get(getPRO().getProperty(getURL()));
		return driver;

	}

	public static WebDriver closeDriver() {

		if (driver != null) {
			driver.close();

		}
		return driver;

	}

}
