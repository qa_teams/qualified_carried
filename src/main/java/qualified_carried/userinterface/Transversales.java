package qualified_carried.userinterface;

import static net.serenitybdd.screenplay.targets.Target.the;

import net.serenitybdd.screenplay.targets.Target;

public class Transversales {

	public static final Target MOUNTAIN_AVATAR = the("Avatar ").locatedBy("//div[@class='company-container']");
	public static final Target PROFILE_MENU = the("Avatar ").locatedBy("//*[contains(text(),'Logout')]");

}
