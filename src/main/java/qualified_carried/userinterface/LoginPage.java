package qualified_carried.userinterface;

import static net.serenitybdd.screenplay.targets.Target.the;
import static org.openqa.selenium.By.id;

import net.serenitybdd.screenplay.targets.Target;

public class LoginPage {

	public static final Target EMAIL = the("User").located(id("mat-input-0"));
	public static final Target PSSWD = the("passwd").located(id("mat-input-1"));
	public static final Target BTN = the("Button").locatedBy("//span[@class='mat-button-wrapper']");
	public static final Target LABEL = the("Label").locatedBy("//*[@class='login-invalid mat-error ng-star-inserted']");

}
