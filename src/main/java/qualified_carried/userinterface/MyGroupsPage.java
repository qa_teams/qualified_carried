package qualified_carried.userinterface;

import static net.serenitybdd.screenplay.targets.Target.the;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.targets.Target;

public class MyGroupsPage {
	public static final Target ADD_NEW_GROUP = the("Add new group").located(By.partialLinkText("Add new group"));
	public static final Target NAME = Target.the("Add new group").locatedBy("//input[@placeholder='Name']");
	public static final Target DESCRIPTION = the("Add new group")
			.locatedBy("//div[@class='mat-form-field-infix']//textarea");
	public static final Target BTN_SAVE = the("Add new group")
			.locatedBy("//div[@class='mat-dialog-actions']//button[2]");
	public static final Target TABLE_TEMPORAL = the("Add new group").locatedBy("//mat-card[@class='mat-card']");

}
