package qualified_carried.userinterface;

import static net.serenitybdd.screenplay.targets.Target.the;

import net.serenitybdd.screenplay.targets.Target;

public class MenuPage {

	public static final Target OPTION_MENU = the("Option menu").locatedBy("//a[.//span[text()='{0}']]");
	public static final Target OPTION_ABS = the("Option menu")
			.locatedBy("//div[@class='ng-tns-c12-6 ng-star-inserted']//div[4]");
	public static final Target OPTION_MY_CARRIERS_ABS = the("Option menu")
			.locatedBy("//div[@class='ng-tns-c12-6 ng-star-inserted']//div[3]");

	public MenuPage() {
		throw new IllegalStateException("Target no encontrado men");
	}

}
