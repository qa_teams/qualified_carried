package qualified_carried.models;

public class DatosGroups {
	
	private String name;
	private String description;
	
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}	

}
