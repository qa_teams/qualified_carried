package qualified_carried.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actions.Click.on;
import static net.serenitybdd.screenplay.actions.Enter.theValue;
import static qualified_carried.userinterface.LoginPage.BTN;
import static qualified_carried.userinterface.LoginPage.EMAIL;
import static qualified_carried.userinterface.LoginPage.PSSWD;
import static qualified_carried.utils.Property.getPRO;
import static qualified_carried.utils.Property.getPassword;
import static qualified_carried.utils.Property.getUsername;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.thucydides.core.annotations.Step;

public class Credentials implements Task {

	@Override
	@Step("{0} Enter credentials")
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(theValue(getPRO().getProperty(getUsername())).into(EMAIL),
				theValue(getPRO().getProperty(getPassword())).into(PSSWD), on(BTN)

		);

	}

	public static Credentials credentials() {
		return instrumented(Credentials.class);
	}

}
