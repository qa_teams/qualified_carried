package qualified_carried.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static qualified_carried.userinterface.MenuPage.OPTION_MENU;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

public class Menu implements Task {

	private String menuOption;

	public Menu(String menuOption) {
		this.menuOption = menuOption;
	}

	@Override
	@Step("{0} the user can select any option #option")
	public <T extends Actor> void performAs(T actor) {

		actor.attemptsTo(Click.on(OPTION_MENU.of(menuOption)));
	}

	public static Menu menu(String menuOption) {
		return instrumented(Menu.class, menuOption);

	}

}
