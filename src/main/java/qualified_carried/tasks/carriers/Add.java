
package qualified_carried.tasks.carriers;

import static net.serenitybdd.screenplay.actions.Click.on;
import static net.serenitybdd.screenplay.actions.Enter.theValue;
import static org.openqa.selenium.Keys.ENTER;
import static qualified_carried.userinterface.MenuPage.OPTION_MENU;
import static qualified_carried.userinterface.MyCarrierPage.ADD_CARRIER;
import static qualified_carried.userinterface.MyCarrierPage.ITEM_TABLE;
import static qualified_carried.userinterface.MyCarrierPage.MATH_ICON;
import static qualified_carried.userinterface.MyCarrierPage.OPTION_TEXT;
import static qualified_carried.userinterface.MyCarrierPage.SEARCH_CARRIER_ADD;
import static qualified_carried.utils.Constantes.OPTION_MY_CARRIERS;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import qualified_carried.utils.General;

public class Add implements Task {

	public Add(String carriersName, String optionList) {
		this.carriersName = carriersName;
		this.optionList = optionList;
	}

	@Steps
	private General general;
	private String carriersName = "carrier name";
	private String optionList = "add to list";
	private Actor theActor;

	public Add(General general) {
		this.general = general;
	}

	@Override
	@Step("{0} select the option #option")
	public <T extends Actor> void performAs(T theActor) {
		this.theActor = theActor;
		theActor.attemptsTo(on(OPTION_MENU.of(OPTION_MY_CARRIERS)));
		theActor.attemptsTo(on(ADD_CARRIER), theValue(carriersName).into(SEARCH_CARRIER_ADD).thenHit(ENTER));

		if (onTheList()) {
			theActor.attemptsTo(Click.on(MATH_ICON));
			theActor.attemptsTo(Click.on(OPTION_TEXT.of("Add to" + " " + optionList)));

		}

	}

	public boolean onTheList() {

		if (ITEM_TABLE.of(carriersName).resolveFor(theActor).containsText(carriersName)) {
			return true;
		} else {
			return false;
		}

	}

}
