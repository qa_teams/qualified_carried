package qualified_carried.tasks.carriers;

import static net.serenitybdd.screenplay.actions.Click.on;
import static net.serenitybdd.screenplay.actions.Enter.theValue;
import static org.openqa.selenium.Keys.ENTER;
import static qualified_carried.userinterface.MenuPage.OPTION_MENU;
import static qualified_carried.userinterface.MyCarrierPage.BUTTON_ICON;
import static qualified_carried.userinterface.MyCarrierPage.MODAL_MY_CARRIERS;
import static qualified_carried.userinterface.MyCarrierPage.OPTION_FILTER;
import static qualified_carried.userinterface.MyCarrierPage.SEARCH_CARRIER_GENERAL;
import static qualified_carried.utils.Constantes.OPTION_MY_CARRIERS;
import static qualified_carried.utils.Constantes.OPT_CARRIERS_LIST;
import static qualified_carried.utils.General.ucFirst;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

public class Search implements Task {

	private String carriersName = "carrier name";
	private String optionList = "add to list";
	private final String REMEMBER = "carrier";

	public Search(String optionList, String carriersName) {
		this.optionList = optionList;
		this.carriersName = carriersName;
	}

	@Override
	@Step("{0} filter the carrier")
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(on(OPTION_MENU.of(OPTION_MY_CARRIERS)), Click.on(BUTTON_ICON));
		if (optionList.contains(OPT_CARRIERS_LIST)) {
			actor.attemptsTo(on(OPTION_FILTER.of("My" + " " + ucFirst(optionList).replace("l", "L"))));
			actor.attemptsTo(on(MODAL_MY_CARRIERS));
			actor.remember(REMEMBER, carriersName);
			actor.attemptsTo(theValue(actor.recall(REMEMBER).toString()).into(SEARCH_CARRIER_GENERAL).thenHit(ENTER));
		} else {
			actor.attemptsTo(Click.on(OPTION_FILTER.of("My" + " " + ucFirst(optionList).substring(0, 8))));
			// Fix ----
		}

	}

}
