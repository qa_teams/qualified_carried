package qualified_carried.tasks.carriers;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class MyCarriers {

	public static Add add(String carriersName, String optionList) {
		return instrumented(Add.class, carriersName, optionList);

	}

	public static Search search(String carriersName, String optionList) {
		return instrumented(Search.class, carriersName, optionList);
	}

}
