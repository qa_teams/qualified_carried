package qualified_carried.tasks.transversales;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actions.Click.on;
import static qualified_carried.userinterface.Transversales.PROFILE_MENU;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.thucydides.core.annotations.Step;

public class Logout implements Task {

	@Override
	@Step("{0} End to session")
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(on(PROFILE_MENU));

	}

	public static Logout logout() {
		return instrumented(Logout.class);

	}

}
