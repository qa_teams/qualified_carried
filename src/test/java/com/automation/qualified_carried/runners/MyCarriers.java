
package com.automation.qualified_carried.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/my_carriers.feature", /* tags = "@carrierList", */ glue = "com\\automation\\qualified_carried\\stepdefinitions")
public class MyCarriers {

}
