package com.automation.qualified_carried.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static qualified_carried.questions.Fond.encontrado;
import static qualified_carried.questions.TheMessage.content;
import static qualified_carried.tasks.carriers.MyCarriers.add;
import static qualified_carried.tasks.carriers.MyCarriers.search;
import static qualified_carried.tasks.transversales.Logout.logout;
import static qualified_carried.utils.Constantes.ACTOR;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MyCarriersStepDefinitions {

	@When("^add a carrier (.*) to his list (.*)$")
	public void addCarrier(String carriersName, String optionList) {
		theActorCalled(ACTOR).attemptsTo(add(carriersName, optionList));

	}

	@And("^he should to visualize a confirmation message (.*)$")
	public void searchCarrier(String message) {
		theActorInTheSpotlight().should(seeThat(content(message), equalTo(message)));

	}

	@Then("^he should display the name (.*) of the carrier after the search (.*)$")
	public void nameCarrierAfterSearch(String optionList, String carriersName) {
		theActorInTheSpotlight().attemptsTo(search(optionList, carriersName));
		theActorInTheSpotlight().should(seeThat(encontrado(carriersName), equalToIgnoringCase(carriersName)));
		theActorInTheSpotlight().attemptsTo(logout());

	}

}
