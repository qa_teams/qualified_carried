package com.automation.qualified_carried.stepdefinitions;

import static net.serenitybdd.screenplay.abilities.BrowseTheWeb.with;
import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static qualified_carried.drivers.MyChromeDriver.closeDriver;
import static qualified_carried.drivers.MyChromeDriver.initChromeDriver;
import static qualified_carried.tasks.Credentials.credentials;
import static qualified_carried.tasks.Menu.menu;
import static qualified_carried.utils.Constantes.ACTOR;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class GeneralStepDefinitions {

	@Before
	public void initBrowser() {
		setTheStage(new OnlineCast());
		theActorCalled(ACTOR).can(BrowseTheWeb.with(initChromeDriver()));
		theActorInTheSpotlight().attemptsTo(credentials());

	}

	@After
	public void closeDrive() {
		theActorInTheSpotlight().can(with(closeDriver()));
	}

	@When("^se selecciona la opcion (.*)$")
	public void seSeleccionaLaOpcion(String menuOption) {
		theActorInTheSpotlight().attemptsTo(menu(menuOption));

	}

}
