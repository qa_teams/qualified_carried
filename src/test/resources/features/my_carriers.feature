Feature: Add my carriers
  
  As a user QC
  I want to  add a carrier

  @carrierList
  Scenario Outline: Successful and failed confirmation
    When add a carrier <carrier name> to his list <add to list>
    Then he should to visualize a confirmation message <confirmation message>
    And he should display the name <add to list> of the carrier after the search <carrier name>

    Examples: 
      | carrier name  | add to list  | confirmation message |
      | efrain castro | carrier list | Something went wrong |
      | efrain castro | carrier list | correcto             |

  @prospectList
  Scenario Outline: Successful and failed confirmation
    When add a carrier <carrier name> to his list <add to list>
    Then he should to visualize a confirmation message <confirmation message>
    And he should display the name <add to list> of the carrier after the search <carrier name>

    Examples: 
      | carrier name       | add to list   | confirmation message |
      | walter donald gray | prospect list | correcto             |
      | walter donald gray | prospect list | Something went wrong |
